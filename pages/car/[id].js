import Head from 'next/head';

import axios from 'axios';
import moment from 'moment';
import { useState } from 'react';
import { useRouter } from 'next/router';
import 'moment/locale/he';
import Link from 'next/link';
import dynamic from 'next/dynamic';

const Carousell = dynamic(() => import('../../components/Carousel'), { ssr: false });

const Car = ({ data, context }) => {
  const [search, setSearch] = useState('');
  const router = useRouter();
  const searchCar = (e) => {
    e.preventDefault();
    router.push({ pathname: `/`, query: { query: search } });
  };

  return (
    <>
      <Head>
        <title>
          carfinds | {data?.maker} {data?.model}
        </title>
      </Head>
      <section className='p-3 border-gray-200 border-b'>
        <div className='container mx-auto flex items-center justify-center'>
          <form className='flex items-center rounded overflow-hidden ' style={{ width: '500px' }} onSubmit={(e) => searchCar(e)}>
            <div className='relative w-full'>
              <span className='absolute inset-y-0 left-0 flex items-center pl-2'>
                <button type='submit' title='Search' className='p-1 focus:outline-none focus:ring'>
                  <svg fill='currentColor' viewBox='0 0 512 512' className='w-4 h-4 text-coolGray-100'>
                    <path d='M479.6,399.716l-81.084-81.084-62.368-25.767A175.014,175.014,0,0,0,368,192c0-97.047-78.953-176-176-176S16,94.953,16,192,94.953,368,192,368a175.034,175.034,0,0,0,101.619-32.377l25.7,62.2L400.4,478.911a56,56,0,1,0,79.2-79.195ZM48,192c0-79.4,64.6-144,144-144s144,64.6,144,144S271.4,336,192,336,48,271.4,48,192ZM456.971,456.284a24.028,24.028,0,0,1-33.942,0l-76.572-76.572-23.894-57.835L380.4,345.771l76.573,76.572A24.028,24.028,0,0,1,456.971,456.284Z'></path>
                  </svg>
                </button>
              </span>
              <input value={search} onChange={(e) => setSearch(e.target.value)} style={{ height: '50px' }} type='search' name='Search' placeholder='חיפוש...' className='w-full bg-gray-100  py-2 pl-10 pr-2 text-sm rounded-md focus:outline-none bg-coolGray-800 text-coolGray-100 focus:bg-coolGray-900 ' />
            </div>
          </form>
        </div>
      </section>

      <section className='container mx-auto p-10 md:py-20 px-0 md:p-10 md:px-0 md:pt-5 px-2'>
        <div className='grid md:grid-cols-3 md:gap-4'>
          <div className='c-card block bg-white border borer-gray-200  rounded-lg overflow-hidden p-2 sticky top-3 mt-10'>
            <h1 className='mt-2 mb-2  font-bold'>
              {data.maker} {data.model}
            </h1>
            <Carousell images={data.images.map((image) => ({ image, maker: data.maker, model: data.model }))} />
            <span className='inline-block  leading-none  text-gray-500 rounded-full font-regular uppercase tracking-wide text-xs'>{moment(data.time).fromNow()}</span>
            <p className=' overflow-ellipsis text-gray-900 overflow-hidden '>{data.text}</p>
            <div className='mt-3 flex items-center'>
              <span className='font-bold text-xl'>{(data.price && data.price.toLocaleString()) || ''}</span>&nbsp;<span className='text-sm font-semibold'>₪</span>
            </div>
            <div className='p-4 border-t mt-3 text-xs text-gray-700 flex justify-center items-center'>
              <span className='flex items-center flex-1'>
                <i className='far fa-clock fa-fw mr-2 text-gray-900'></i>שנה {data.year || '-'}
              </span>
              <span className='flex items-center flex-1'>
                <i className='far fa-address-card fa-fw text-gray-900 mr-2'></i> יד {data.hand || '-'}
              </span>
              <span className='flex items-center flex-1'>
                <i className='far fa-address-card fa-fw text-gray-900 mr-2'></i> ק״מ {(data.km && data.km.toLocaleString()) || '-'}
              </span>
            </div>
            <div className='flex justify-start items-center mt-3 '>
              <div>
                <Link href={`/api/car-link?to=${data.id}`}>
                  <a rel='nofollow' target='_blank' type='button' className='text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-3 md:mr-0  mt-0'>
                    צפייה במודעה
                  </a>
                </Link>
              </div>
            </div>
          </div>

          <div className='col-span-2'></div>
        </div>
      </section>
    </>
  );
};

export async function getServerSideProps(context) {
  //   console.log({ context });
  const id = context?.params?.id;
  const { data } = await axios.get(`https://9i255s1e7g.execute-api.eu-central-1.amazonaws.com/dev/car/${id}`);
  console.log({ data });
  return {
    props: { data: data }, // will be passed to the page component as props
  };
}

export default Car;
