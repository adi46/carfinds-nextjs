import Head from 'next/head';
import Image from 'next/image';
import styles from '../styles/Home.module.css';
import axios from 'axios';
import moment from 'moment';
import { useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';

import 'moment/locale/he';
const Home = ({ data, context }) => {
  const [search, setSearch] = useState('');
  const router = useRouter();
  const searchCar = (e) => {
    e.preventDefault();
    router.push({ pathname: `/`, query: { query: search } });
  };

  return (
    <>
      <Head>
        <title>carfinds | {router.query?.maker}</title>
      </Head>
      <section className='p-3 border-gray-200 border-b'>
        <div className='container mx-auto flex items-center justify-center'>
          <form className='flex items-center rounded overflow-hidden ' style={{ width: '500px' }} onSubmit={(e) => searchCar(e)}>
            <div className='relative w-full'>
              <span className='absolute inset-y-0 left-0 flex items-center pl-2'>
                <button type='submit' title='Search' className='p-1 focus:outline-none focus:ring'>
                  <svg fill='currentColor' viewBox='0 0 512 512' className='w-4 h-4 text-coolGray-100'>
                    <path d='M479.6,399.716l-81.084-81.084-62.368-25.767A175.014,175.014,0,0,0,368,192c0-97.047-78.953-176-176-176S16,94.953,16,192,94.953,368,192,368a175.034,175.034,0,0,0,101.619-32.377l25.7,62.2L400.4,478.911a56,56,0,1,0,79.2-79.195ZM48,192c0-79.4,64.6-144,144-144s144,64.6,144,144S271.4,336,192,336,48,271.4,48,192ZM456.971,456.284a24.028,24.028,0,0,1-33.942,0l-76.572-76.572-23.894-57.835L380.4,345.771l76.573,76.572A24.028,24.028,0,0,1,456.971,456.284Z'></path>
                  </svg>
                </button>
              </span>
              <input value={search} onChange={(e) => setSearch(e.target.value)} style={{ height: '50px' }} type='search' name='Search' placeholder='חיפוש...' className='w-full bg-gray-100  py-2 pl-10 pr-2 text-sm rounded-md focus:outline-none bg-coolGray-800 text-coolGray-100 focus:bg-coolGray-900 ' />
            </div>
          </form>
        </div>
      </section>

      <section className='container mx-auto p-10 md:py-20 px-0 md:p-10 md:px-0 md:pt-5'>
        <div className='grid md:grid-cols-3 md:gap-4 '>
          <div className='col-span-2'>
            <p className='text-gray-500 text-sm'>תוצאות {data.total}</p>

            <section className='md:p-5 md:p-0 grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 xl:grid-cols-3 2xl:grid-cols-3 gap-5 items-start '>
              {data.feed.map((car) => (
                <Link href={`/car/${car.id}`} key={car.id}>
                  <div key={car.id} className='w-full  p-4'>
                    <a className='c-card block bg-white border borer-gray-200  rounded-lg overflow-hidden cursor-pointer'>
                      <div className='relative pb-48 overflow-hidden'>
                        <img layout='fill' className='absolute inset-0 h-full w-full object-cover' src={car.images[0]} alt='' />
                      </div>
                      <div className='p-4'>
                        <span className='inline-block  leading-none  text-gray-500 rounded-full font-regular uppercase tracking-wide text-xs'>{moment(car.time).fromNow()}</span>
                        <h2 className='mt-2 mb-2  font-bold'>
                          {car.maker} {car.model}
                        </h2>
                        <p className='text-sm overflow-ellipsis text-gray-900 overflow-hidden h-12' style={{ height: '40px' }}>
                          {car.text}
                        </p>
                        <div className='mt-3 flex items-center'>
                          <span className='font-bold text-xl'>{(car.price && car.price.toLocaleString()) || ''}</span>&nbsp;<span className='text-sm font-semibold'>₪</span>
                        </div>
                      </div>
                      <div className='p-4 border-t  text-xs text-gray-700 flex justify-center items-center'>
                        <span className='flex items-center flex-1'>
                          <i className='far fa-clock fa-fw mr-2 text-gray-900'></i>שנה {car.year || '-'}
                        </span>
                        <span className='flex items-center flex-1'>
                          <i className='far fa-address-card fa-fw text-gray-900 mr-2'></i> יד {car.hand || '-'}
                        </span>
                        <span className='flex items-center flex-1'>
                          <i className='far fa-address-card fa-fw text-gray-900 mr-2'></i> ק״מ {(car.km && car.km.toLocaleString()) || '-'}
                        </span>
                      </div>
                    </a>
                  </div>
                </Link>
              ))}
            </section>
          </div>
          <div></div>
        </div>
      </section>
    </>
  );
};

export async function getServerSideProps(context) {
  // console.log({ context });
  const query = context?.params?.maker;
  const data = await axios.get(`https://9i255s1e7g.execute-api.eu-central-1.amazonaws.com/dev/search?query=${encodeURIComponent(query)}`);
  // console.log({ data });
  return {
    props: { data: data.data }, // will be passed to the page component as props
  };
}

export default Home;
