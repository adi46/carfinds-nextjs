// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const elasticsearch = require('elasticsearch');
const client = new elasticsearch.Client({
  host: '35.158.110.61:9200',
  log: 'error',
  // apiVersion: '7.2', // use the same version of your Elasticsearch instance
});

export default async function handler(req, res) {
  const response = await client.search({
    index: 'cars-*',
    body: {
      query: {
        bool: {
          filter: {
            term: {
              _id: req.query.to,
            },
          },
        },
      },
    },
  });
  console.log({ response });
  const item = response.hits.hits[0];
  let link = '';
  if (item._source.data_source === 'yad2') {
    link = 'https://www.yad2.co.il/item/' + item._source.id;
  } else {
    link = item._source.post_url;
  }
  return res.redirect(link);
}
