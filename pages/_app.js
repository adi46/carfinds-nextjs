import 'tailwindcss/tailwind.css';
import '../styles/globals.css';
import Link from 'next/link';
import axios from 'axios';
import App from 'next/app';
import Script from 'next/script';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

function MyApp({ Component, pageProps, makers }) {
  console.log({ pageProps });
  const router = useRouter();

  const handleRouteChange = (url) => {
    window.gtag('config', 'UA-213467349-1', {
      page_path: url,
    });
  };

  useEffect(() => {
    router.events.on('routeChangeComplete', handleRouteChange);
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange);
    };
  }, [router.events]);

  return (
    <>
      <nav className='bg-white border-gray-200 px-2 p-3 border-gray-200 border-b'>
        <div className='container mx-auto flex flex-wrap items-center justify-between'>
          <Link href='/' className='flex'>
            <a className='self-center text-2xl font-extrabold whitespace-nowrap uppercase tracking-tighter'>
              Car<span className='text-blue-700'>Finds</span>
            </a>
          </Link>
          <div className='flex md:order-2 hidden'>
            <button type='button' className='text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-3 md:mr-0'>
              כניסת סחורים
            </button>
            <button data-collapse-toggle='mobile-menu-4' type='button' className='md:hidden text-gray-400 hover:text-gray-900 focus:outline-none focus:ring-2 focus:ring-blue-300 rounded-lg inline-flex items-center justify-center' aria-controls='mobile-menu-4' aria-expanded='false'>
              <span className='sr-only'>Open main menu</span>
              <svg className='w-6 h-6' fill='currentColor' viewBox='0 0 20 20' xmlns='http://www.w3.org/2000/svg'>
                <path fillRule='evenodd' d='M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z' clipRule='evenodd'></path>
              </svg>
              <svg className='hidden w-6 h-6' fill='currentColor' viewBox='0 0 20 20' xmlns='http://www.w3.org/2000/svg'>
                <path fillRule='evenodd' d='M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z' clipRule='evenodd'></path>
              </svg>
            </button>
          </div>
        </div>
      </nav>
      <Component {...pageProps} />

      <div className='bg-white border-gray-200 p-5 border-gray-200 border-t'>
        <div className='container mx-auto flex flex-wrap'>
          {makers.map((maker) => (
            <Link href={`/${maker.key}`} key={maker.key}>
              <a className="className='md:mx-4 md:mb-4 ml-2 mb-2 text-sm inline-flex items-center font-bold leading-sm uppercase px-3 py-1 rounded bg-white text-gray-700 border hover:bg-blue-700 hover:text-white'">{maker.key}</a>
            </Link>
          ))}
        </div>
      </div>

      <div className='bg-white border-gray-200 p-5 border-gray-200 border-t'>
        <p className='text-xs'>
          באתר זה מוצגים פריטים שהינם תוצאה של אלגוריתמים לעיבוד מידע אשר אינם חפים מטעויות. יתכן למשל שהתוכנה שלנו תבצע טעות בזיהוי דובר/ת, בפירוש הטקסט, או במדידת ערכים הקשורים בטקסט.
          <br />
          הנהלת האתר אינה נושאת באחריות על טעויות שייתכנו. השימוש באתר ושיתוף המידע שבאתר הוא באחריות המשתמש בלבד.
        </p>
      </div>
    </>
  );
}

MyApp.getInitialProps = async (appContext) => {
  const { data } = await axios.get(`https://9i255s1e7g.execute-api.eu-central-1.amazonaws.com/dev/makers`);

  const { pageProps, ...appProps } = await App.getInitialProps(appContext);

  const result = {
    ...appProps,
    makers: data.makers,
  };

  if (Object.keys(pageProps) > 0) {
    result.pageProps = pageProps;
  }

  console.log({ result });

  return result;
};

export default MyApp;
