import { getServerSideSitemap } from 'next-sitemap';
import { GetServerSideProps } from 'next';
import axios from 'axios';
const elasticsearch = require('elasticsearch');
const client = new elasticsearch.Client({
  host: '35.158.110.61:9200',
  log: 'error',
  // apiVersion: '7.2', // use the same version of your Elasticsearch instance
});
export const getServerSideProps = async (ctx) => {
  // Method to source urls from cms
  // const urls = await fetch('https//example.com/api')

  const routes = [];
  const { data } = await axios.get('https://9i255s1e7g.execute-api.eu-central-1.amazonaws.com/dev/makers-models');
  // console.log(data.makers);
  data.makers.forEach((maker) => {
    routes.push(`https://carfinds.co/${maker.key}`);
    maker.models.buckets.forEach((model) => {
      routes.push(`https://carfinds.co/${maker.key} ${model.key}`);
    });
  });

  const fields = routes.map((route) => ({ loc: route }));
  return getServerSideSitemap(ctx, fields);
};

// Default export to prevent next.js errors
export default () => {};
