import Document, { Html, Head, Main, NextScript } from 'next/document';

class MyDocument extends Document {
  render() {
    return (
      <Html dir='rtl'>
        <Head>
          <link href='https://fonts.googleapis.com' rel='preconnect' />
          <link href='https://fonts.gstatic.com' rel='preconnect' />
          <link href='https://fonts.googleapis.com/css2?family=Assistant:wght@200;300;400;600;800&display=swap' rel='stylesheet' />
          <script async src='https://www.googletagmanager.com/gtag/js?id=UA-213467349-1' />

          <script
            dangerouslySetInnerHTML={{
              __html: `
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
              gtag('config', 'UA-213467349-1', { page_path: window.location.pathname });
            `,
            }}
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
