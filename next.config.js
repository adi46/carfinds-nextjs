module.exports = {
  reactStrictMode: false,
  ignoreBuildErrors: true,
  images: {
    domains: ['car-image-resources.s3.eu-central-1.amazonaws.com', 'img.yad2.co.il'],
  },
};
