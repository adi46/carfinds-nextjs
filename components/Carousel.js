import React, { Component } from 'react';

import Carousel, { autoplayPlugin } from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';
import Image from 'next/image';

export default function Carousell({ images }) {
  return (
    <Carousel
      plugins={[
        'rtl',
        {
          resolve: autoplayPlugin,
          options: {
            interval: 2000,
          },
        },
      ]}
      animationSpeed={1000}
    >
      {images.map((img) => (
        <div className='relative pb-72 overflow-hidden rounded w-full h-full' key={img.image}>
          <img layout='fill' className='absolute inset-0 h-full w-full object-cover' src={img.image} alt={`${img.model} ${img.maker}`} />
        </div>
      ))}
    </Carousel>
  );
}
