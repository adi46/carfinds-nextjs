// next-sitemap.js
module.exports = {
  siteUrl: 'https://carfinds.co',
  generateRobotsTxt: true,
  exclude: ['/server-sitemap.xml'], // <= exclude here
  sitemapSize: 7000,
  robotsTxtOptions: {
    additionalSitemaps: [
      'https://carfinds.co/server-sitemap.xml', // <==== Add here
    ],
  },
};
